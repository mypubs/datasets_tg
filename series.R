
# Script used to generate the datasets of random noises and chaotic maps

# NOTE: all datasets are saved as R's RDS object format: check at
# ?readRDS to verify how to load them

source('utils.R')

# path to the bandt_pompe repository
bp_path='../bandt_pompe'

# loading bandt-pompe functions
loadSource(bp_path, 'bandt_pompe.R')
loadSource(bp_path, 'measures.R')

# loading the time series generator
source('tsgenerator.R')

# parameters
len = 5*10^3
trans = 5000

#set.seed(1234)

# random noises
pts = seq(0, 3.5, by=0.25)

for(k in pts)
{
    # TODO: average of 10 realizations 
    for(j in 1:10)
    {
        res = getSeries('noise', parms=c(k=k), len=len, trans=trans)
        saveRDS(res, 
                file=paste('data/random_k_',k,'_',j,'-series.dat', sep=''))
    }
}

# chaotic maps
vars = c(
        # noninvertible maps
         'logistic', 'sine', 'tent', 'lcg', 'cubic', 'ricker', 
         'gauss', 'cusp', 'pinchers', 'spence', 'sinecircle',
       # dissipative maps
        'henon', 'lozi', 'delayedlogistic', 'tinkerbell', 'burgers',
         'holmes', 'dissipative', 'ikeda', 'sinai', 'predator',
       # conservative maps
        'chirikov', 'henonarea', 'arnold', 'gingerbread', 'web', 'lorenz3d' 
         )

for(i in vars)
{
    res = getSeries(i, len=len)
    saveRDS(res, file=paste('data/chaotic_map_',i,'-series.dat', sep=''))
}


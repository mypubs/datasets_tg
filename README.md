# Datasets and its generation codes #

This repository contains the source code to generate the datasets used in the analysis of the characterization of time series based on the ordinal patterns transition graphs.

NOTE: the datasets are saved as R's RDS objects, check at ?readRDS to
see how to load them.



# for chaotic time series
suppressMessages(library(deSolve))

# for fractal time series
suppressMessages(library(dvfBm))

# for colored noises
source('noisefk.R')
suppressMessages(library(tuneR))

# from Ravetti (2014)

# helper function to generate different time series for the experiments
# on the BP transition graph
getSeries = function(type = 'noise',    # the type of the time series
                     len=1000,          # the length to be returned
                     transient=1000,    # the number of initial values to discard
                     parms = c()        # the list of parameters
                    )
{
    # the length to be generated
    len = len + transient

    # switching on the options for type
    switch(type, 

            # random noises
            # - white: k = 0
            # - pink:  k = 1
            # - red:   k = 2
            # - black: k = 3
            noise = 
            {
                # getting the parameters
                k = checkPar(parms, 'k', 0)
                T = checkPar(parms, 'T', 1)

                # generating the noise
                x = noisefk(N = len, k = k, T=T)
                #x = noise(kind = c("power"), samp.rate=len, alpha=k)@left
            },
            whitenoise = 
            {
                x = noise(kind = c("white"), samp.rate=len)@left
            },
            pinknoise = 
            {
                x = noise(kind = c("pink"), samp.rate=len)@left
            },
            rednoise = 
            {
                x = noise(kind = c("red"), samp.rate=len)@left
            },
            blacknoise = 
            {
                x = noise(kind = c("power"), samp.rate=len, alpha=3)@left
            },
            othernoise = 
            {
                k = checkPar(parms, 'k', 0)
                x = noise(kind = c("power"), samp.rate=len, alpha=k)@left
            },
            # deterministic chaotic
            lorenz = 
            {
                # from deSolve package example
                
                state = c(
                           x = checkPar(parms, 'x0', 1),
                           y = checkPar(parms, 'y0', 1),
                           z = checkPar(parms, 'z0', 1)
                           )
                
                parameters = c(
                               s = checkPar(parms, 's', 10),
                               b = checkPar(parms, 'b', 8/3),
                               r = checkPar(parms, 'r', 28)
                               )

                hop = checkPar(parms, 'hop', 0.01)
                times = seq(hop, len*hop, by=hop)

                # lorenz ordinary differential equations
                Lorenz = function(t, state, parameters) 
                {
                    with(as.list(c(state, parameters)), {
                        dx = s * (y - x)
                        dy = x * (r - z) - y
                        dz = x * y - b * z
                        list(c(dx, dy, dz))
                    })
                }

                out = ode(y = state, times = times, func = Lorenz, 
                          parms = parameters, method = 'rk4')

                times = out[,'time']
                x = out[,'x']
                y = out[,'y']
                z = out[,'z']

                #p = lorenz(time = seq(hop, len*hop, by=hop), 
                #           do.plot=FALSE)
            },
            rossler = 
            {
                state = c(
                           x = checkPar(parms, 'x0', 1),
                           y = checkPar(parms, 'y0', 1),
                           z = checkPar(parms, 'z0', 1)
                           )
                
                parameters = c(
                               a = checkPar(parms, 'a', 0.2),
                               b = checkPar(parms, 'b', 0.2),
                               c = checkPar(parms, 'c', 5.7)
                               )
                
                hop = checkPar(parms, 'hop', 0.01)
                times = seq(hop, len*hop, by=hop)
                
                # Rossler ordinary differential equations
                Rossler = function(t, state, parameters) 
                {
                    with(as.list(c(state, parameters)), {
                        dx = -(y + z)
                        dy = x + a * y
                        dz = b + z * (x - c)
                        list(c(dx, dy, dz))
                    })
                }
                
                out = ode(y = state, times = times, func = Rossler, 
                          parms = parameters, method = 'rk4')

                times = out[,'time']
                x = out[,'x']
                y = out[,'y']
                z = out[,'z']
                
                #p = rossler(time = seq(hop, len*hop, by=hop), 
                #            do.plot=FALSE)
            },

            # chaotic maps

            # (a) noninvertible maps: 
            
            # (1) logistic map 
            logistic = 
            {
                r = checkPar(parms, 'r', 4)
                x0 = checkPar(parms, 'x0', 0.1)
                
                x = rep(0, len)
                x[1] = x0
                
                for(n in 1:(len-1))
                {
                    x[n+1] = r * x[n] * (1 - x[n])
                }
                #x = logisticMap(n.sample=len, start=0.8, r=4, 
                #                n.transient=0, do.plot=F)
            },
            # (2) sine map 
            sine = 
            {
                a = checkPar(parms, 'a', 1)
                x0 = checkPar(parms, 'x0', 0.1)
                
                x = rep(0, len)
                x[1] = x0
                
                for(n in 1:(len-1))
                {
                    x[n+1] = a * sin(pi * x[n])
                }
            },
            # (3) tent map 
            tent = 
            {
                a = checkPar(parms, 'a', 2)
                x0 = checkPar(parms, 'x0', 1/sqrt(2))
                
                # to handle the round error in R
                # from: fNonlinear package
                if (a == 2) 
                {
                    a = a - .Machine$double.eps
                }

                x = rep(0, len)
                x[1] = x0
                
                for(n in 1:(len-1))
                {
                    x[n+1] = a * min(x[n], 1-x[n])
                    #x[n+1] = (a/2) * (1 - 2 * abs(x[n] - 0.5))
                }
            },
            # (4) linear congruential generator
            lcg =
            {
                a = checkPar(parms, 'a', 7141)
                b = checkPar(parms, 'b', 54773)
                c = checkPar(parms, 'c', 259200)
                x0 = checkPar(parms, 'x0', 0)

                x = rep(0, len)
                x[1] = x0
                
                for(n in 1:(len-1))
                {
                    x[n+1] = (a * x[n] + b) %% c
                    # TODO: check if this division is necessary
                    #x[n+1] = x[n+1]/c
                }
            },
            # (5) cubic map
            cubic =
            {
                a = checkPar(parms, 'a', 3)
                x0 = checkPar(parms, 'x0', 0.1)
                
                x = rep(0, len)
                x[1] = x0
                
                for(n in 1:(len-1))
                {
                    x[n+1] = a * x[n] * (1 - x[n]^2)
                    # TODO: check this other equation
                    #x[n+1] = a * x[n]^3 + (1 - a)*x[n]
                }
            },
            # (6) Ricker's population model
            ricker =
            {
                r = checkPar(parms, 'r', 3) # 2.692
                k = checkPar(parms, 'k', 200)
                a = checkPar(parms, 'a', 20)
                x0 = checkPar(parms, 'x0', 0.1)
                
                x = rep(0, len)
                x[1] = x0
                
                for(n in 1:(len-1))
                {
                    x[n+1] = a * x[n] * exp(-x[n])
                    # TODO: check this equation
                    # from:
                    # https://www.ma.utexas.edu/users/davis/375/ \
                    #   popecol/lec9/chaos.html
                    #x[n+1] = x[n] * exp( r * ( 1 - x[n]/k ) )
                }
            },
            # (7) Gauss map
            gauss = 
            {
                x0 = checkPar(parms, 'x0', 0.099999999)
            
                x = rep(0, len)
                x[1] = x0
                
                for(n in 1:(len-1))
                {
                    x[n+1] = (1/x[n]) %% 1
                }
            },
            # (8) Cusp map 
            cusp =
            {
                a = checkPar(parms, 'a', 2)
                b = checkPar(parms, 'b', 1.1)
                x0 = checkPar(parms, 'x0', 0.5)
            
                x = rep(0, len)
                x[1] = x0
                
                for(n in 1:(len-1))
                {
                    x[n+1] = 1 - a * sqrt( abs(x[n]) )
                    # TODO: check this
                    #x[n+1] = a - b * sqrt( abs(x[n]) )
                }
            },
            # (9) Pinchers map 
            pinchers =
            {
                s = checkPar(parms, 's', 2)
                c = checkPar(parms, 'c', 0.5)
                x0 = checkPar(parms, 'x0', 0)
            
                x = rep(0, len)
                x[1] = x0
                
                for(n in 1:(len-1))
                {
                    x[n+1] = abs( tanh( s * (x[n] - c) ) )
                }
            },
            # (10) Spence map 
            spence = 
            {
                x0 = checkPar(parms, 'x0', 0.5)
            
                x = rep(0, len)
                x[1] = x0
                
                for(n in 1:(len-1))
                {
                    x[n+1] = abs( log( x[n] ) )
                }
            },
            # (11) sine-circle map
            sinecircle = 
            {
                a = checkPar(parms, 'a', 0.5)
                k = checkPar(parms, 'k', 2)
                x0 = checkPar(parms, 'x0', 0.1)
            
                x = rep(0, len)
                x[1] = x0
                
                for(n in 1:(len-1))
                {
                    x[n+1] = ( x[n] + a - k/(2*pi) * sin( 2 * pi * x[n] ) ) %% 1
                }
            },
            
            # (b) dissipative maps: 

            # (12) Henon map
            henon = 
            {
                a = checkPar(parms, 'a', 1.4)
                b = checkPar(parms, 'b', 0.3)
                x0 = checkPar(parms, 'x0', 0)
                y0 = checkPar(parms, 'y0', 0.9)

                x = rep(0, len)
                x[1] = x0
                
                y = rep(0, len)
                y[1] = y0

                for(n in 1:(len-1))
                {
                    x[n+1] = 1 - a * x[n]^2 + b * y[n]
                    y[n+1] = x[n]
                }
            },
            # (13) Lozi map 
            lozi = 
            {
                a = checkPar(parms, 'a', 1.7)
                b = checkPar(parms, 'b', 0.5)
                x0 = checkPar(parms, 'x0', -0.1)
                y0 = checkPar(parms, 'y0', 0.1)

                x = rep(0, len)
                x[1] = x0
                
                y = rep(0, len)
                y[1] = y0

                for(n in 1:(len-1))
                {
                    x[n+1] = 1 - a * abs( x[n] ) + b * y[n]
                    y[n+1] = x[n]
                }
            },
            # (14) delayed logistic map 
            delayedlogistic =
            {
                a = checkPar(parms, 'a', 2.27)
                x0 = checkPar(parms, 'x0', 0.001)
                y0 = checkPar(parms, 'y0', 0.001)

                x = rep(0, len)
                x[1] = x0
                
                y = rep(0, len)
                y[1] = y0

                for(n in 1:(len-1))
                {
                    x[n+1] = a * x[n] * (1 - y[n])
                    y[n+1] = x[n]
                }
            },
            # (15) Tinkerbell map 
            tinkerbell =
            {
                a = checkPar(parms, 'a', 0.9)
                b = checkPar(parms, 'b', -0.6)
                c = checkPar(parms, 'c', 2)
                d = checkPar(parms, 'd', 0.5)
                x0 = checkPar(parms, 'x0', 0)
                y0 = checkPar(parms, 'y0', 0.5)

                x = rep(0, len)
                x[1] = x0
                
                y = rep(0, len)
                y[1] = y0

                for(n in 1:(len-1))
                {
                    x[n+1] = x[n]^2 - y[n]^2 + a * x[n] + b * y[n]
                    y[n+1] = 2 * x[n] * y[n] + c * x[n] + d * y[n]
                }
            },
            # (16) Burgers' map 
            burgers =
            {
                a = checkPar(parms, 'a', 0.75)
                b = checkPar(parms, 'b', 1.75)
                x0 = checkPar(parms, 'x0', -0.1)
                y0 = checkPar(parms, 'y0', 0.1)

                x = rep(0, len)
                x[1] = x0
                
                y = rep(0, len)
                y[1] = y0

                for(n in 1:(len-1))
                {
                    #x[n+1] = a * x[n]^2 - y[n]^2
                    # from Senkerik (2014)
                    x[n+1] = a * x[n] - y[n]^2
                    y[n+1] = b * y[n] + x[n] * y[n]
                }
            },
            # (17) Holmes cubic map 
            holmes =
            {
                b = checkPar(parms, 'b', 0.2)
                d = checkPar(parms, 'd', 2.77)
                x0 = checkPar(parms, 'x0', 1.6)
                y0 = checkPar(parms, 'y0', 0)

                x = rep(0, len)
                x[1] = x0
                
                y = rep(0, len)
                y[1] = y0

                for(n in 1:(len-1))
                {
                    x[n+1] = y[n]
                    y[n+1] = -b * x[n] + d * y[n] - y[n]^3
                }
            },
            # (18) dissipative standard map 
            dissipative =
            {
                b = checkPar(parms, 'b', 0.1)
                k = checkPar(parms, 'k', 8.8)
                x0 = checkPar(parms, 'x0', 0.1)
                y0 = checkPar(parms, 'y0', 0.1)

                x = rep(0, len)
                x[1] = x0
                
                y = rep(0, len)
                y[1] = y0

                for(n in 1:(len-1))
                {
                    # from paper
                    y[n+1] = ( b * y[n] + k * sin(x[n]) ) %% (2*pi)
                    x[n+1] = ( x[n] + y[n+1] ) %% (2*pi)
                }
            },
            # (19) Ikeda map 
            ikeda =
            {
                a = checkPar(parms, 'a', 6)
                b = checkPar(parms, 'b', 0.4)
                g = checkPar(parms, 'g', 1)
                m = checkPar(parms, 'm', 0.9)
                x0 = checkPar(parms, 'x0', 0)
                y0 = checkPar(parms, 'y0', 0)

                x = rep(0, len)
                x[1] = x0
                
                y = rep(0, len)
                y[1] = y0

                for(n in 1:(len-1))
                {
                    phi = b - a / (1 + x[n]^2 + y[n]^2)
                    x[n+1] = g + m * ( x[n] * cos(phi) - y[n] * sin(phi) )
                    y[n+1] = m * (x[n] * sin(phi) + y[n] * cos(phi) )
                }
            },
            # (20) Sinai map 
            sinai =
            {
                d = checkPar(parms, 'd', 0.1)
                x0 = checkPar(parms, 'x0', 0.5)
                y0 = checkPar(parms, 'y0', 0.5)

                x = rep(0, len)
                x[1] = x0
                
                y = rep(0, len)
                y[1] = y0

                for(n in 1:(len-1))
                {
                    x[n+1] = ( x[n] + y[n] + d * cos(2 * pi * y[n]) ) %% 1
                    y[n+1] = ( x[n] + 2 * y[n] ) %% 1
                }
            },
            # (21) discrete predator-prey map
            predator =
            {
                r = checkPar(parms, 'r', 3)
                k = checkPar(parms, 'k', 1)
                a = checkPar(parms, 'a', 5)
                x0 = checkPar(parms, 'x0', 0.5)
                y0 = checkPar(parms, 'y0', 0.5)

                x = rep(0, len)
                x[1] = x0
                
                y = rep(0, len)
                y[1] = y0

                for(n in 1:(len-1))
                {
                    x[n+1] = x[n] * exp( r * ( 1 - x[n]/k ) - a * y[n] )
                    y[n+1] = x[n] * ( 1 - exp( -a * y[n] ) )
                }
            },

            # (c) conservative maps:
            
            # (22) Chirikov standard map 
            chirikov =
            {
                k = checkPar(parms, 'k', 1)
                x0 = checkPar(parms, 'x0', 0)
                y0 = checkPar(parms, 'y0', 6)

                x = rep(0, len)
                x[1] = x0
                
                y = rep(0, len)
                y[1] = y0

                for(n in 1:(len-1))
                {
                    y[n+1] = ( y[n] + k * sin(x[n]) ) %% (2*pi)
                    x[n+1] = ( x[n] + y[n+1] ) %% (2*pi)
                }
            },
            # (23) Henon area-preserving quadratic map 
            henonarea =
            {
                a = checkPar(parms, 'a', acos(0.24))
                x0 = checkPar(parms, 'x0', 0.6)
                y0 = checkPar(parms, 'y0', 0.13)

                x = rep(0, len)
                x[1] = x0
                
                y = rep(0, len)
                y[1] = y0

                for(n in 1:(len-1))
                {
                    x[n+1] = x[n] * cos(a) - (y[n] - x[n]^2) * sin(a)
                    y[n+1] = x[n] * sin(a) + (y[n] - x[n]^2) * cos(a)
                }
            },
            # (24) Arnold's cat map 
            arnold =
            {
                k = checkPar(parms, 'k', 2)
                x0 = checkPar(parms, 'x0', 0)
                y0 = checkPar(parms, 'y0', 1/sqrt(2))

                x = rep(0, len)
                x[1] = x0
                
                y = rep(0, len)
                y[1] = y0

                for(n in 1:(len-1))
                {
                    x[n+1] = ( x[n] + y[n] ) %% 1
                    y[n+1] = ( x[n] + k * y[n] ) %% 1
                }
            },
            # (25) Gingerbreadman map 
            gingerbread =
            {
                x0 = checkPar(parms, 'x0', 0.5)
                y0 = checkPar(parms, 'y0', 3.7)

                x = rep(0, len)
                x[1] = x0
                
                y = rep(0, len)
                y[1] = y0

                for(n in 1:(len-1))
                {
                    x[n+1] = 1 + abs(x[n]) - y[n]
                    y[n+1] = x[n]
                }
            },
            # (26) chaotic web map 
            web =
            {
                # TODO: check if it is correct, the exmaple figure is
                # not similar to the paper
                a = checkPar(parms, 'a', pi/2)
                k = checkPar(parms, 'k', 1)
                x0 = checkPar(parms, 'x0', 0)
                y0 = checkPar(parms, 'y0', 3)

                x = rep(0, len)
                x[1] = x0
                
                y = rep(0, len)
                y[1] = y0

                for(n in 1:(len-1))
                {
                    x[n+1] = x[n] * cos(a) - (y[n] + k * sin(x[n])) * sin(a)
                    y[n+1] = x[n] * sin(a) + (y[n] + k * sin(x[n])) * cos(a)
                }
            },
            # (27) Lorenz three-dimensional chaotic map
            lorenz3d =
            {
                x0 = checkPar(parms, 'x0', 0.5)
                y0 = checkPar(parms, 'y0', 0.5)
                z0 = checkPar(parms, 'z0', -1)

                x = rep(0, len)
                x[1] = x0
                
                y = rep(0, len)
                y[1] = y0
                
                z = rep(0, len)
                z[1] = z0

                for(n in 1:(len-1))
                {
                    x[n+1] = x[n] * y[n] - z[n]
                    y[n+1] = x[n]
                    z[n+1] = y[n]
                }
            },

            # default case
            {
                x = 1:len
            }
        )


    res = list()

    if ( exists('times') )
    {
        res$times = times[(transient+1):len]
    }
    
    if ( exists('x') )
    {
        res$x = x[(transient+1):len]
    }
    
    if ( exists('y') )
    {
        res$y = y[(transient+1):len]
    }

    if ( exists('z') )
    {
        res$z = z[(transient+1):len]
    }

    # discarding the transient when returning
    return(res)
}

# check for a parameter or set a default in case of missing
checkPar = function(parms, name, default)
{
    if (name %in% names(parms))
    {
        return( as.numeric(parms[name]) )
    }
    else
    {
        return(default)
    }
}

